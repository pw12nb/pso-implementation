﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PSO_Implementation
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtOutput.Clear();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            int swarm = Convert.ToInt16(txtSwarmSize.Text);
            double inertia = Convert.ToDouble(txtInertia.Text);
            double personal = Convert.ToDouble(txtInertia.Text);
            double global = Convert.ToDouble(txtGlobal.Text);

            Swarm s = new Swarm(cmbFunctions.SelectedIndex, txtOutput, swarm, inertia, personal, global, true);
            s.PSO();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
