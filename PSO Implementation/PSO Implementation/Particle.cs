﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace PSO_Implementation
{
    class Particle
    {
        private double position;
        private double velocity;
        private double pBest;
        private double fitness;

        public Particle(double initialPosition)
        {
            this.position = initialPosition;
        }

        public double Fitness
        {
            get { return fitness; }
            set { fitness = value; }
        }

        public double Position
        {
            get { return position; }
            set { position = value; }
        }

        public double PBest
        {
            get { return pBest; }
            set { pBest = value; }
        }

        public double Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        public bool UpdatePersonalBest(double newFitness, double newPosition, bool min)
        {
            //if the pBest hasn't been set or the new position is better
            //update the pBest value
            if ((fitness == null || pBest == null) ||
                (newFitness < fitness && min) ||
                (newFitness > fitness && !min))
            {
                fitness = newFitness;
                pBest = position;
                return true;
            }
            return false;
        }
    }
}
