﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PSO_Implementation
{
    class Swarm
    {
        Particle[] particles;
        TextBox output;
        int functionToPerform;
        Particle globalBest;
        bool min = true;
        int loops;
        bool problemSatisfied = false;
        int swarmSize;
        double C1 = 1;
        double C2 = 1;
        double INERTIA = 1;

        public Swarm(int functionToPerform, TextBox output, int swarmSize, double C1, double C2, double INERTIA, bool min)
        {
            this.functionToPerform = functionToPerform;
            this.output = output;
            this.min = min;
            this.swarmSize = swarmSize;
            this.C1 = C1;
            this.C2 = C2;
            this.INERTIA = INERTIA;
        }

        private void InitializeParticles()
        {
            globalBest = null;
            particles = new Particle[swarmSize];
            Random rand = new Random();
            for (int i = 0; i < swarmSize; i++)
            {
                particles[i] = new Particle((int)(rand.NextDouble() * 100) - 50);
            }
        }

        //Update the velocity of the points based on the velocity solution
        private void UpdateVelocity(Particle p)
        {
            Random random = new Random();
            double R1 = random.NextDouble();
            double R2 = random.NextDouble();

            p.Velocity = (INERTIA * p.Velocity) +
                         C1 * R1 * (p.PBest - p.Position) +
                         C2 * R2 * (globalBest.Position - p.Position);
        }

        //Use the velocity to update the position
        private void UpdatePosition(Particle p)
        {
            p.Position = p.Position + p.Velocity;
        }

        //calculate the fitness of the PSO
        public void PSO()
        {
            InitializeParticles();
            int loopsSinceNoNewGlobalBest = 0;
            while (!problemSatisfied)
            {
                foreach (Particle i in particles)
                {
                    double fitness = CalculateFitness(i.Position);
                    if (i.UpdatePersonalBest(fitness, i.Position, min))
                        writeOutput("New Particle " + i + " best: x=" + i.Position);
                    if ((globalBest == null) ||
                        (i.Fitness < globalBest.Fitness && min) ||
                        (globalBest.Fitness < i.Fitness && !min))
                    {

                        globalBest = i;
                        loopsSinceNoNewGlobalBest = 0;
                        writeOutput("New Particle " + i + " best: x=" + i.Position);
                    }
                }
                foreach (Particle i in particles)
                {
                    UpdateVelocity(i);
                    UpdatePosition(i);
                }

                if (++loopsSinceNoNewGlobalBest == 5) 
                    problemSatisfied = true;
            }
            writeOutput("Final Best X Value is...."+globalBest.Position);
        }

        private double CalculateFitness(double position)
        {
            switch (functionToPerform)
            {
                case 0: return (3 - Math.Pow(position, 2));
            }
            return 0;
                
        }

        private void writeOutput(String output)
        {
            this.output.AppendText("\n" + output);
        }

    }
}
