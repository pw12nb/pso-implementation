﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSO_Implementation
{
    class Coordinates
    {
        double x;
        double y;

        public Coordinates(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public double X
        {
            get { return x; }
            set { x = value; }
        }

        public double Y
        {
            get { return y; }
            set { y = value; }
        }

        
    }
}
